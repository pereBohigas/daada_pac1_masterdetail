package edu.uoc.daada_pac1_masterdetail.model

import java.util.*

class BookItem (val identificator: Int, val title: String, val author: String, val publicationDate: Date, val description: String, val urlImage: String) {

    override fun toString(): String {
        return description.toString()
    }

}